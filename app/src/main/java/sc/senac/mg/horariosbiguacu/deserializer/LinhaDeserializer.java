package sc.senac.mg.horariosbiguacu.deserializer;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

import sc.senac.mg.horariosbiguacu.entity.Horario;
import sc.senac.mg.horariosbiguacu.entity.Linha;

public class LinhaDeserializer implements JsonDeserializer<ArrayList<Linha>> {

    @Override
    public ArrayList<Linha> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        ArrayList<Linha> linhasResult = new ArrayList<>();

        JsonArray linhas = json.getAsJsonArray();

        for (JsonElement l : linhas) {
            JsonObject lObject = l.getAsJsonObject();

            Linha linha = new Linha();
            linha.setCodigo(lObject.get("codigo").getAsInt());
            linha.setLinha(lObject.get("linha").getAsString());
            linha.setOrigem(lObject.get("origem").getAsString());
            linha.setDestino(lObject.get("destino").getAsString());
            linha.setObservacao(lObject.get("observacao").getAsString());

            JsonObject informacoes = lObject.getAsJsonObject("informations");
            linha.setVlrTarifa(informacoes.get("vlr_tarifa").getAsString());
            linha.setTempoViagem(informacoes.get("tempo_viagem").getAsString());
            linha.setUltimaAtualizacao(informacoes.get("ultima_atualizacao").getAsString());

            JsonObject oHorarios = lObject.getAsJsonObject("horarios");
            JsonObject horariosOrigem = oHorarios.getAsJsonObject(linha.getOrigem());
            JsonObject horariosDestino = oHorarios.getAsJsonObject(linha.getDestino());

            ArrayList<Horario> horarios = new ArrayList<>();

            Horario h = new Horario();
            h.setIdentificador(linha.getOrigem());
            ArrayList<String> hOrigemSemana = parseHorarios(horariosOrigem, "semana");
            ArrayList<String> hOrigemSabado = parseHorarios(horariosOrigem, "sabado");
            ArrayList<String> hOrigemDomingo = parseHorarios(horariosOrigem, "domingo");

            h.setSemana(hOrigemSemana);
            h.setSabado(hOrigemSabado);
            h.setDomingo(hOrigemDomingo);
            horarios.add(h);

            h = new Horario();
            h.setIdentificador(linha.getDestino());
            ArrayList<String> hDestinoSemana = parseHorarios(horariosDestino, "semana");
            ArrayList<String> hDestinoSabado = parseHorarios(horariosDestino, "sabado");
            ArrayList<String> hDestinoDomingo = parseHorarios(horariosDestino, "domingo");

            h.setSemana(hDestinoSemana);
            h.setSabado(hDestinoSabado);
            h.setDomingo(hDestinoDomingo);
            horarios.add(h);

            linha.setHorarios(horarios);

            linhasResult.add(linha);
        }

        return linhasResult;
    }

    public ArrayList<String> parseHorarios(JsonObject source, String tipo) {

        ArrayList<String> horariosFinal = new ArrayList<>();

        JsonArray horarios = source.getAsJsonArray(tipo);

        for (JsonElement el : horarios) {
            horariosFinal.add(el.getAsString());
        }
        return horariosFinal;
    }
}
