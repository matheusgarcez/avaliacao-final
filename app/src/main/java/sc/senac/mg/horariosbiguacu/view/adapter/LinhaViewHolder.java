package sc.senac.mg.horariosbiguacu.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import sc.senac.mg.horariosbiguacu.R;
import sc.senac.mg.horariosbiguacu.entity.Linha;

public class LinhaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    public TextView nomeLinhaView;
    public TextView valorTarifaView;
    public TextView tempoViagemView;

    public TextView labelOrigem;
    public TextView proximoHorarioOrigem;

    public TextView labelDestino;
    public TextView proximoHorarioDestino;

    private ClickListener clickListener;

    public LinhaViewHolder(View itemView, ClickListener clickListener) {
        super(itemView);

        this.clickListener = clickListener;

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);

        this.nomeLinhaView = (TextView) itemView.findViewById(R.id.nomeLinha);
        this.valorTarifaView = (TextView) itemView.findViewById(R.id.valorTarifa);
        this.tempoViagemView = (TextView) itemView.findViewById(R.id.tempoViagemView);

        this.labelDestino = (TextView) itemView.findViewById(R.id.labelDestino);
        this.labelOrigem = (TextView) itemView.findViewById(R.id.labelOrigem);
    }

    public void update(Linha model) {

        this.nomeLinhaView.setText(model.getLinha() + (model.getObservacao().isEmpty() ? "" : " (" + model.getObservacao() + ")"));
        this.valorTarifaView.setText("Tarifa: " + model.getVlrTarifa());

        this.labelDestino.setText("Destino: " + model.getDestino());
        this.labelOrigem.setText("Origem: " + model.getOrigem());
        this.tempoViagemView.setText("Tempo de viagem: " + model.getTempoViagem());

    }

    @Override
    public void onClick(View v) {
        if (clickListener != null)
            clickListener.onItemClick(getAdapterPosition(), v);
    }

    @Override
    public boolean onLongClick(View v) {
        return clickListener == null || clickListener.onItemLongClick(getAdapterPosition(), v);
    }
}