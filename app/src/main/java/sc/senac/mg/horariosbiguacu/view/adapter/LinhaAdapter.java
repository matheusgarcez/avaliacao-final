package sc.senac.mg.horariosbiguacu.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sc.senac.mg.horariosbiguacu.R;
import sc.senac.mg.horariosbiguacu.entity.Linha;

public class LinhaAdapter extends RecyclerView.Adapter<LinhaViewHolder> {

    private ClickListener clickListener;
    private List<Linha> mLinhaModel;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }


    public LinhaAdapter(List<Linha> listModel) {
        this.mLinhaModel = listModel;
    }

    @Override
    public void onBindViewHolder(LinhaViewHolder viewHolder, int position) {
        Linha model = mLinhaModel.get(position);
        viewHolder.update(model);
    }

    @Override
    public LinhaViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.linha_item, viewGroup, false);
        return new LinhaViewHolder(v, this.clickListener);
    }

    public Linha getItem(int position) {
        return mLinhaModel.get(position);
    }

    public List<Linha> getItemList() {
        return this.mLinhaModel;
    }

    @Override
    public int getItemCount() {
        return mLinhaModel.size();
    }

    public void updateList(List<Linha> listModel) {
        this.mLinhaModel.clear();
        this.mLinhaModel.addAll(listModel);
        notifyDataSetChanged();
    }
}