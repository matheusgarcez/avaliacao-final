package sc.senac.mg.horariosbiguacu.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import sc.senac.mg.horariosbiguacu.R;
import sc.senac.mg.horariosbiguacu.entity.Horario;
import sc.senac.mg.horariosbiguacu.entity.Linha;

public class HorarioActivity extends AppCompatActivity implements View.OnClickListener {

    public Linha linha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario);

        linha = (Linha) getIntent().getExtras().getSerializable("linha");

        TextView linhaView = (TextView) findViewById(R.id.nomeLinha);
        linhaView.setText(String.format("%s%s", linha.getLinha(), linha.getObservacao().isEmpty() ? "" : " (" + linha.getObservacao() + ")"));

        TextView vlrTarifaView = (TextView) findViewById(R.id.vlrTarifa);
        vlrTarifaView.setText(String.format("Tarifa: %s", linha.getVlrTarifa()));

        TextView tempoViagemView = (TextView) findViewById(R.id.tempoViagemView);
        tempoViagemView.setText(String.format("Tempo de viagem: %s", linha.getTempoViagem()));

        TextView labelOrigem = (TextView) findViewById(R.id.labelOrigem);
        TextView labelDestino = (TextView) findViewById(R.id.labelDestino);

        LinearLayout containerDestino = (LinearLayout) findViewById(R.id.containerHorariosDestino);
        LinearLayout containerOrigem = (LinearLayout) findViewById(R.id.containerHorariosOrigem);

        labelOrigem.setText(linha.getOrigem());
        labelDestino.setText(linha.getDestino());

        Horario horarioOrigem = linha.horarios.get(linha.horarios.indexOf(new Horario(linha.getOrigem())));
        Horario horarioDestino = linha.horarios.get(linha.horarios.indexOf(new Horario(linha.getDestino())));

        ArrayList<String> horariosDestino;
        ArrayList<String> horariosOrigem;

        Calendar c = Calendar.getInstance();
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        if (dayOfWeek == Calendar.SATURDAY) {
            horariosDestino = horarioDestino.getSabado();
            horariosOrigem = horarioOrigem.getSabado();
        } else if (dayOfWeek == Calendar.SUNDAY) {
            horariosDestino = horarioDestino.getDomingo();
            horariosOrigem = horarioOrigem.getDomingo();
        } else {
            horariosDestino = horarioDestino.getSemana();
            horariosOrigem = horarioOrigem.getSemana();
        }

        for (String horario : horariosDestino) {
            TextView h = new TextView(this);
            h.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            h.setTextSize(20);
            h.setPadding(10, 10, 10, 10);
            h.setText(horario);
            containerDestino.addView(h);
        }

        for (String horario : horariosOrigem) {
            TextView h = new TextView(this);
            h.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            h.setTextSize(20);
            h.setPadding(10, 10, 10, 10);
            h.setText(horario);
            containerOrigem.addView(h);
        }

        findViewById(R.id.btnAvisarHorario).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        final Date dataAtual = new Date();
        final DateFormat formatter = new SimpleDateFormat("HHmm");

        Horario horarioOrigem = linha.horarios.get(linha.horarios.indexOf(new Horario(linha.getOrigem())));
        Calendar c = Calendar.getInstance();

        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        ArrayList<String> horariosOrigem;
        if (dayOfWeek == Calendar.SATURDAY) {
            horariosOrigem = horarioOrigem.getSabado();
        } else if (dayOfWeek == Calendar.SUNDAY) {
            horariosOrigem = horarioOrigem.getDomingo();
        } else {
            horariosOrigem = horarioOrigem.getSemana();
        }

        Integer horaAtual = Integer.parseInt(formatter.format(dataAtual));
        String horaMaisAproximada = null;

        for (String horario : horariosOrigem) {

            Integer hora = Integer.parseInt(horario.replace(":", ""));

            if (horaMaisAproximada == null && hora <= horaAtual) {
                horaMaisAproximada = horario;
            } else if (Integer.parseInt(horaMaisAproximada.replace(":", "")) < hora && hora >= horaAtual) {
                horaMaisAproximada = horario;
                break;
            }

        }

        if (horaMaisAproximada != null) {

            Intent it = new Intent(this, NotificationService.class);
            it.putExtra("linha", linha);
            it.putExtra("tempo", horaMaisAproximada);

            stopService(it);
            startService(it);
        }
    }
}
