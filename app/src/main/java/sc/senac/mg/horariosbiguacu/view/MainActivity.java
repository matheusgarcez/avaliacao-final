package sc.senac.mg.horariosbiguacu.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import sc.senac.mg.horariosbiguacu.Application;
import sc.senac.mg.horariosbiguacu.R;
import sc.senac.mg.horariosbiguacu.entity.Linha;
import sc.senac.mg.horariosbiguacu.deserializer.LinhaDeserializer;
import sc.senac.mg.horariosbiguacu.view.adapter.ClickListener;
import sc.senac.mg.horariosbiguacu.view.adapter.LinhaAdapter;

public class MainActivity extends AppCompatActivity implements MenuItemCompat.OnActionExpandListener, Drawer.OnDrawerItemClickListener {

    private static final int MENU_ITEM_LINHAS = 1;
    private static final int MENU_ITEM_SOBRE = 2;
    private static final int MENU_ITEM_SAIR = 3;

    private final OkHttpClient client = new OkHttpClient();

    public ArrayList<Linha> linhas;

    private Application application;
    private ActionBar toolbar;
    private Drawer menuLateral;

    private RecyclerView recyclerView;
    private LinhaAdapter linhaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.toolbar = getSupportActionBar();
        this.application = (Application) getApplication();

        // Inicializa o menu lateral
        menuLateral = new DrawerBuilder()
            .withActivity(this)
            .withToolbar(toolbar)
            .withRootView(R.id.drawer_layout)
            .withDisplayBelowStatusBar(true)
            .withTranslucentStatusBar(false)
            .withActionBarDrawerToggleAnimated(true)
            .addDrawerItems(
                new PrimaryDrawerItem()
                    .withName(R.string.linhas)
                    .withIdentifier(MENU_ITEM_LINHAS),
                new DividerDrawerItem(),
                new PrimaryDrawerItem()
                    .withName(R.string.sobre)
                    .withIdentifier(MENU_ITEM_SOBRE)
                    .withSelectable(false),
                new PrimaryDrawerItem()
                    .withName(R.string.sair)
                    .withIdentifier(MENU_ITEM_SAIR)
                    .withSelectable(false)
            )
            // Restaura o estado da barra lateral caso o usuário mude a orientação da tela
            .withSavedInstance(savedInstanceState)
            .withOnDrawerItemClickListener(this)
            .build();

        this.linhas = new ArrayList<>();

        this.recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerView.scrollToPosition(0);

        this.linhaAdapter = new LinhaAdapter(this.linhas);

        this.linhaAdapter.setOnItemClickListener(new ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent it = new Intent(MainActivity.this, HorarioActivity.class);
                Linha l = linhas.get(position);
                it.putExtra("linha", l);
                startActivity(it);
            }

            @Override
            public boolean onItemLongClick(int position, View v) {
                return false;
            }
        });

        this.recyclerView.setAdapter(this.linhaAdapter);

        carregarLinhasOnibus();
    }

    private void carregarLinhasOnibus() {

        Request request = new Request.Builder()
            .url("https://api.myjson.com/bins/1z1cn")
            .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code " + responseBody);


                    Type collectionType = new TypeToken<List<Linha>>() {}.getType();

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.registerTypeAdapter(collectionType, new LinhaDeserializer());

                    Gson gson = gsonBuilder.create();
                    String json = responseBody.source().readUtf8();

                    linhas = gson.fromJson(json, collectionType);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            linhaAdapter.updateList(linhas);
                        }
                    });

                }
            }
        });
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        menuLateral.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
        toolbar.setDisplayHomeAsUpEnabled(false);
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {

        toolbar.setDisplayHomeAsUpEnabled(false);
        menuLateral.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);

        return true;
    }

    public void mostrarInformacoesAplicativo() {

        new MaterialDialog.Builder(this)
            .title("Sobre")
            .content(Html.fromHtml(
                new StringBuilder()
                    .append("Aplicativo desenvolvido como trabalho final da disciplina de desenvolvimento para aplicativos móveis.<br/><br/>")
                    .append("<b>Desenvolvido por:</b> <br/><br/>")
                    .append("Matheus Vitória Garcez")
                    .toString()
                )
            )
            .positiveText(getString(R.string.fechar).toUpperCase())
            .show();
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

        drawerItem.withSetSelected(false);

        switch ((int) drawerItem.getIdentifier()) {
            case MENU_ITEM_SOBRE:
                mostrarInformacoesAplicativo();
                break;
            case MENU_ITEM_SAIR:

                new MaterialDialog.Builder(this)
                    .title("Sair")
                    .content("Você têm certeza que deseja sair do aplicativo?")
                    .positiveText(getString(R.string.sim).toUpperCase())
                    .negativeText(getString(R.string.nao).toUpperCase())
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            finish();
                        }
                    })
                    .show();

                break;
            default:
                break;
        }

        return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState = menuLateral.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (menuLateral != null && menuLateral.isDrawerOpen()) {
            menuLateral.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }
}
