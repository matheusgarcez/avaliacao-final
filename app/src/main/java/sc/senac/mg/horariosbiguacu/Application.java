package sc.senac.mg.horariosbiguacu;

import sc.senac.mg.horariosbiguacu.manager.HorarioManager;

public class Application extends android.app.Application {

    private HorarioManager horarioManager;
    private static Application mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        this.horarioManager = new HorarioManager();
    }

}
