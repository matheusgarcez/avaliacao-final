package sc.senac.mg.horariosbiguacu.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Horario implements Serializable {

    public String identificador;
    public ArrayList<String> semana;
    public ArrayList<String> sabado;
    public ArrayList<String> domingo;

    public Horario() {
    }

    public Horario(String identificador) {
        this.identificador = identificador;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Horario horario = (Horario) o;
        return Objects.equals(identificador, horario.identificador);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identificador);
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public ArrayList<String> getSemana() {
        return semana;
    }

    public void setSemana(ArrayList<String> semana) {
        this.semana = semana;
    }

    public ArrayList<String> getSabado() {
        return sabado;
    }

    public void setSabado(ArrayList<String> sabado) {
        this.sabado = sabado;
    }

    public ArrayList<String> getDomingo() {
        return domingo;
    }

    public void setDomingo(ArrayList<String> domingo) {
        this.domingo = domingo;
    }
}