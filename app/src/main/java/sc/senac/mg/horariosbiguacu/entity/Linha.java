package sc.senac.mg.horariosbiguacu.entity;

import android.text.format.Time;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public final class Linha implements Serializable {

    public Integer codigo;
    public String linha;
    public String origem;
    public String destino;
    public String observacao;
    public String vlrTarifa;
    public String tempoViagem;
    public String ultimaAtualizacao;

    public ArrayList<Horario> horarios;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Linha linha = (Linha) o;
        return Objects.equals(codigo, linha.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }

    public Linha() {
    }

    public Linha(Integer codigo, String linha, String origem, String destino, String observacao, String vlrTarifa, String tempoViagem, String ultimaAtualizacao, ArrayList<Horario> horarios) {
        this.codigo = codigo;
        this.linha = linha;
        this.origem = origem;
        this.destino = destino;
        this.observacao = observacao;
        this.vlrTarifa = vlrTarifa;
        this.tempoViagem = tempoViagem;
        this.ultimaAtualizacao = ultimaAtualizacao;
        this.horarios = horarios;
    }

    @Override
    public String toString() {
        return String.format("%s%s", codigo, linha);
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getVlrTarifa() {
        return vlrTarifa;
    }

    public void setVlrTarifa(String vlrTarifa) {
        this.vlrTarifa = vlrTarifa;
    }

    public String getTempoViagem() {
        return tempoViagem;
    }

    public void setTempoViagem(String tempoViagem) {
        this.tempoViagem = tempoViagem;
    }

    public String getUltimaAtualizacao() {
        return ultimaAtualizacao;
    }

    public void setUltimaAtualizacao(String ultimaAtualizacao) {
        this.ultimaAtualizacao = ultimaAtualizacao;
    }

    public ArrayList<Horario> getHorarios() {
        return horarios;
    }

    public void setHorarios(ArrayList<Horario> horarios) {
        this.horarios = horarios;
    }
}
