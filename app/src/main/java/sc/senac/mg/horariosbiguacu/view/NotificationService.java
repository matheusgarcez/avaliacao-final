package sc.senac.mg.horariosbiguacu.view;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import sc.senac.mg.horariosbiguacu.R;
import sc.senac.mg.horariosbiguacu.entity.Linha;

public class NotificationService extends Service {

    private Timer mTimer;
    private Intent it;
    private Linha linha;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            this.it = intent;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (this.it != null) {

            String[] tempo = this.it.getStringExtra("tempo").split(":");
            linha = (Linha) this.it.getSerializableExtra("linha");

            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR, Integer.valueOf(tempo[0]));
            c.set(Calendar.MINUTE, Integer.valueOf(tempo[1]) - 5);
            c.set(Calendar.SECOND, 0);

            mTimer = new Timer();
            mTimer.schedule(timerAction, c.getTime());
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private TimerTask timerAction = new TimerTask() {
        @Override
        public void run() {
            notifiy();
            this.cancel();
            mTimer.cancel();
        }
    };

    @Override
    public void onDestroy() {

        try {
            mTimer.cancel();
            timerAction.cancel();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Intent intent = new Intent("sc.senac.mg.horariosbiguacu.linha_alarm");
        intent.putExtra("finish", "true");
        sendBroadcast(intent);
    }

    public void notifiy() {

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("LinhaService");

        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("sc.senac.mg.horariosbiguacu"));
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, myIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
        Context context = getApplicationContext();

        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle("Ônibus chegando!")
                .setContentText("Faltam menos de 5 minutos para a chegada do ônibus '" + linha.getLinha() + "'")
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.app_icon);

        Notification notification = builder.build();

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, notification);

    }
}
